import { Platform } from 'ionic-angular';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';

import { AppService } from '../../app/app.service';

declare var webserver: any;

@Injectable()
export class WebserverService extends AppService {

  public serverStatus = new Subject<any>();
  serverStatus$ = this.serverStatus.asObservable();

  constructor() {
    super();
  }

  startServer(port) {
    webserver.start(
      () => {
        console.info('Server started @ port: ' + port);
        this.serverStatus.next('Running');
      },
      () => {
        console.error('Server fail to start!');
        this.serverStatus.next('Stop');
      },
      port
    )
  }

  stopServer() {
    webserver.stop();
  }

  initiateListen() {
    webserver.onRequest(
      (request) => {
        console.log("Request accepted ****************************** | " + JSON.stringify(request));

        let requestId = request.requestId;
        let params = request.query;

        if (params) {
          let data = {
            success: true,
            message: 'Server Running!'
          };
          let statusCode = 200;
          let cleanParams = this.recodeParams(params);

          // Needed Action
          // Save Customer - from jr (first_name, last_name, gender, mobile, email, bithdate, input_time or kahit sayo na to??)
          // Fetch Customer - to jr (name, input time)
          // Login - from jr (cuid, username, password)
           
          if ( cleanParams.action == 'add_customer' ) {
            
          } else if (cleanParams.action == 'fetch_customer') {

          } else if (cleanParams.action == 'login') {

          }

          this.sendResponse(requestId, statusCode, data);
        }
        
      }
    )
  }

  sendResponse(requestId: string, status: number, data: any) {
    console.log(requestId);
    webserver.sendResponse(
      requestId,
      {
        status: status,
        body : data,
        headers : {
          'Content-Type' : 'application/json'
        }
      }
    )
  }

  recodeParams(params) {

    let cleanParams = <any>{};
    params = params.split('&');

    params.forEach((element, index) => {
      let chars = element.split('=');
      cleanParams[chars[0]] = chars[1];
    });

    return cleanParams;
  }
}
