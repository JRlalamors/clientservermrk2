/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { WebserverService } from './webserver.service';

describe('Service: Webserver', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [WebserverService]
    });
  });

  it('should ...', inject([WebserverService], (service: WebserverService) => {
    expect(service).toBeTruthy();
  }));
});
