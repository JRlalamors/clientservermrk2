import { Component, OnInit } from '@angular/core';
import { NavController, Platform } from 'ionic-angular';

import { NetworkInterface } from '@ionic-native/network-interface';
import { WebserverService } from './../../shared/webserver/webserver.service';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage implements OnInit {

  carrierIp = null;
  deviceIp = null;
  port = 5000;
  serverStatus = null;

  constructor(
    private networkInterface: NetworkInterface,
    public navCtrl: NavController,
    private serverService: WebserverService,
    private platform: Platform) {
    
    this.networkInterface.getCarrierIPAddress()
      .then(address => this.carrierIp = address.ip)
      .catch(error => console.error(`Unable to get IP: ${error}`));

    this.networkInterface.getWiFiIPAddress()
      .then(address => this.deviceIp = address.ip)
      .catch(error => console.error(`Unable to get IP: ${error}`));

    this.serverService.serverStatus$.subscribe(status => {
      this.serverStatus = status;
    });
  }

  ngOnInit() {

    // Start the server
    this.platform.ready().then(() => {
      this.serverService.startServer(this.port);

      // Initiate server listener
      this.serverStatus = this.serverService.initiateListen();
    });
  }  
}
